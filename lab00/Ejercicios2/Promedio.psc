Proceso promedio
	Definir  n,i Como Entero;
	Definir acumulador,dato Como Real;
	Escribir "¿Cuantos datos quiere ingresar?";
	Leer n;
	acumulador<-0;
	Para i<-1 hasta n Hacer
		Escribir "Ingrese el ",i," dato";
		Leer dato;
		acumulador<-acumulador+dato;
	FinPara
	
	Escribir "El promedio es: ", acumulador/n;
FinProceso
