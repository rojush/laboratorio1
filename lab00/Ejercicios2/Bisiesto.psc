Proceso anioBisiesto
	Definir anio Como Entero;
	Definir bisiesto Como Logico;
	Escribir "Ingrese el a�o";
	Leer anio;
	bisiesto<-((anio%4=0 Y anio%100<>0) O anio%400=0);
	
	si bisiesto Entonces
		Escribir "Es a�o bisiesto";
	SiNo
		Escribir "El a�o no es bisiesto";
	FinSi

FinProceso
