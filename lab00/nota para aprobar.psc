Proceso Nota
	Definir T1,T2,T3,NL,NF Como Real;
	NF <- 60;
	Repetir;
		Escribir "Ingrese la primera calificacion del Teorico";
		Leer T1;
	Hasta Que T1>=0 y T1<=100;
	Repetir
		Escribir "Ingrese la segunda calificacion del Teorico";
		Leer T2;
	Hasta Que T2>=0 y T2<=100;
	Repetir
		Escribir "Ingrese la nota del laboratorio";
		Leer NL;
	Hasta Que NL>=0 y NL<=100;
	T3 <- (60-NL*0.3)*(3/0.7)-T1-T2;
	Si T3<=100 Entonces
		Escribir "SeNecesita ",T3," para aprobar";
	SiNo
		Escribir "No hay nota que alcance reprobaste";
	FinSi
FinProceso