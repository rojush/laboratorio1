Proceso numeroPi
	Definir  i, n Como Entero;
	Definir sumatoria Como Real;
	Escribir "Ingrese cantidad de aproximacion";
	Leer n;
	sumatoria <- 0;
	Para i<-1 hasta n con paso 1 Hacer
		sumatoria <- sumatoria - ((-1)^i)/(i*2-1);
	FinPara
	Escribir 4*sumatoria;
FinProceso